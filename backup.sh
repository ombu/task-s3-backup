#!/bin/sh

set -e

    echo "Backup task started"

if [ -z "${GROUP}" ] || [ -z "${FILE_PATH}" ]; then
    echo "Backup task failed"
    exit 1
fi

chgrp -R ${GROUP} ${FILE_PATH}
find ${FILE_PATH} . -type f -exec chmod 0660 {} \;
find ${FILE_PATH} . -type d -exec chmod 2770 {} \;

exclude_paths=""
if [ -n "${EXCLUDE}" ]; then
    exclude_paths="--exclude ${EXCLUDE//,/ --exclude }"
fi

if [ "${BACKUP_BUCKET}" ]; then
    aws s3 sync --delete --sse AES256 ${exclude_paths} ${FILE_PATH} s3://${BACKUP_BUCKET}
fi

echo "Backup task completed"

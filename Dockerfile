FROM alpine:3.7

MAINTAINER OMBU

RUN apk update && \
    apk add py-pip && \
    pip install awscli && \
    addgroup -g 1000 -S www-data && \
    adduser -u 1000 -D -S -G www-data www-data

COPY backup.sh /tmp

RUN chmod +x /tmp/backup.sh

WORKDIR /tmp

CMD ["./backup.sh"]
